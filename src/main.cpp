#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "linmath.h"

#include <stdlib.h>
#include <stdio.h>

#include <vect.hpp>

using namespace std;

static const struct
{
    float x, y;
    float r, g, b;
} vertices[] =
{
    { -1.0f, -1.0f, 0.f, 0.f, 1.f },
    { -1.0f,  1.0f, 0.f, 0.f, 1.f },
    {  1.0f, -1.0f, 0.f, 0.f, 1.f },

    { -1.0f,  1.0f, 1.f, 0.f, 0.f },
    {  1.0f,  1.0f, 1.f, 0.f, 0.f },
    {  1.0f, -1.0f, 1.f, 0.f, 0.f },
};

string readFile(string const& fileName)
{
    ifstream t(fileName);
    return string(istreambuf_iterator<char>(t), istreambuf_iterator<char>());
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

GLchar errorLog[1 << 20];
GLint createShader(string const& fileName, GLenum shaderType)
{
    string shaderSrc = readFile(fileName);
    GLint shader = glCreateShader(shaderType);

    const char* shaderSrcPtr = shaderSrc.c_str();
    glShaderSource(shader, 1, &shaderSrcPtr, NULL);
    glCompileShader(shader);
    
    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        vector<char> errorLog(maxLength);
        glGetShaderInfoLog(shader, maxLength, &maxLength, errorLog.data());
        cerr << errorLog.data() << endl;

        glDeleteShader(shader); 
        exit(-1);
    }

    return shader;
}

using vec = vector<float>;

int const N = 3;
float const V = 0.3 * 10;

// vect<3> pos{0, 0, -20};
// vect<3> dir{0, 0, 1};
// vect<3> up {0, 1, 0};
// vect<3> rig{-1, 0, 0};

vector<vect<4>> poss, dirs, ups, rigs, d4dims;

vect<4> pos{0, 0, 0, 0};

vect<4> dir{0, 0, 1, 0};
vect<4> up{0, 1, 0, 0};
vect<4> rig{-1, 0, 0, 0};
vect<4> d4dim{0, 0, 0, 1};

bool coloringFlag = true;
bool reflectionFlag = false;

const float KEY_SPEED = 0.04;

map<int, bool> key_pressed
    {
        make_pair (GLFW_KEY_UP, false),
        make_pair (GLFW_KEY_DOWN, false),
        make_pair (GLFW_KEY_LEFT, false),
        make_pair (GLFW_KEY_RIGHT, false),
        make_pair (GLFW_KEY_W, false),
        make_pair (GLFW_KEY_A, false),
        make_pair (GLFW_KEY_S, false),
        make_pair (GLFW_KEY_D, false)
    };

void loadPositions(size_t num)
{
    pos   = poss[num];
    dir   = dirs[num];
    up    = ups[num];
    rig   = rigs[num];
    d4dim = d4dims[num];
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_F11 && action == GLFW_PRESS) {
        coloringFlag = !coloringFlag;
    }
    if (key == GLFW_KEY_F12 && action == GLFW_PRESS) {
        reflectionFlag = !reflectionFlag;
    }
    if (GLFW_KEY_F1 <= key && key <= GLFW_KEY_F10 && action == GLFW_PRESS) {
        loadPositions(key - GLFW_KEY_F1);
    }

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, 1);
        printf("esc %d action %d\n", key, action);
    }

    key_pressed[key] = action != GLFW_RELEASE;
    printf("key %d action %d\n", key, action);
}

static bool key_check(GLFWwindow* window)
{
    vect<4> delta;
    bool changed = false;

    if (key_pressed[GLFW_KEY_UP] || key_pressed[GLFW_KEY_W]) {
        delta += dir;
        printf("%d ", GLFW_KEY_UP);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_DOWN] || key_pressed[GLFW_KEY_S]) {
        delta -= dir;
        printf("%d ", GLFW_KEY_DOWN);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_LEFT] || key_pressed[GLFW_KEY_A]) {
        delta += rig;
        printf("%d ", GLFW_KEY_LEFT);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_RIGHT] || key_pressed[GLFW_KEY_D]) {
        delta -= rig;
        printf("%d ", GLFW_KEY_RIGHT);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_SPACE] || key_pressed[GLFW_KEY_D]) {
        delta += up;
        printf("%d ", GLFW_KEY_RIGHT);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_LEFT_SHIFT] || key_pressed[GLFW_KEY_D]) {
        delta -= up;
        printf("%d ", GLFW_KEY_RIGHT);
        changed = true;
    }
    if (key_pressed[GLFW_KEY_Z]) {
        delta -= d4dim * 0.3;
    }
    if (key_pressed[GLFW_KEY_C]) {
        delta += d4dim * 0.3;
    }
    if (key_pressed[GLFW_KEY_Q]) {
        simpleRotation(rig, d4dim, KEY_SPEED);
    }
    if (key_pressed[GLFW_KEY_E]) {
        simpleRotation(rig, d4dim, -KEY_SPEED);
    }

    cout << "pos : "   << pos[0]   << ' ' << pos[1]   << ' ' << pos[2]   << ' ' << pos[3]   << endl;
    cout << "dir : "   << dir[0]   << ' ' << dir[1]   << ' ' << dir[2]   << ' ' << dir[3]   << endl;
    cout << "up : "    << up[0]    << ' ' << up[1]    << ' ' << up[2]    << ' ' << up[3]    << endl;
    cout << "rig : "   << rig[0]   << ' ' << rig[1]   << ' ' << rig[2]   << ' ' << rig[3]   << endl;
    cout << "d4dim : " << d4dim[0] << ' ' << d4dim[1] << ' ' << d4dim[2] << ' ' << d4dim[3] << endl;

    pos += (delta * KEY_SPEED) * V;
    return changed;
}

void cursor_pos_callback(GLFWwindow* window, double x, double y)
{
    static double prevX = x;
    static double prevY = y;

    double dx =  (x - prevX) * 0.001;
    double dy = -(y - prevY) * 0.001;

    simpleRotation(dir, rig, dx);
    simpleRotation(up, dir, dy);

    prevX = x;
    prevY = y; 
}

enum class ObjectType
{
    Sphere,
    Torus,
    Box,
    HardTorus,
    SpheroCube,
};

struct SphereData
{
    static constexpr int size = 1;

    float r;
    void loadValue(GLint location)
    {
        glUniform1f(location, r);
    }
};

struct TorusData
{
    static constexpr int size = 1;

    float t1, t2;
    void loadValue(GLint location)
    {
        glUniform2f(location, t1, t2);
    }
};

struct SpheroCubeData
{
    static constexpr int size = 1;

    float t1, t2, t3;
    void loadValue(GLint location)
    {
        glUniform3f(location, t1, t2, t3);
    }
};

struct BoxData
{
    static constexpr int size = 1;

    float b1, b2, b3, b4;
    void loadValue(GLint location)
    {
        glUniform4f(location, b1, b2, b3, b4);
    }
};

struct ObjectData
{
    static constexpr int size = 3;
    ObjectType objectType;
    int objectId;
    float dx, dy, dz, dw;

    void loadValue(GLint location)
    {
        glUniform1i(location + 0, (int) objectType);
        glUniform1i(location + 1, objectId);        
        glUniform4f(location + 2, dx, dy, dz, dw);      
    }
};

template<typename T>
void loadArrayOfStructs(vector<T> data, GLint location)
{
    for (size_t i = 0; i < data.size(); i++) {
        data[i].loadValue(location + i * T::size);
    }
} 

float myrand()
{
    return (float) random() / RAND_MAX;
}

vector<SphereData> spheres;
vector<TorusData> toruses;
vector<BoxData> boxes;
vector<ObjectData> objects;

void readScene(string const& fileName)
{
    ifstream inp(fileName);

    size_t cnt;

    inp >> cnt;
    for (size_t i = 0; i < cnt; i++) {
        float r;
        inp >> r;
        spheres.push_back({r});
    }

    inp >> cnt;
    for (size_t i = 0; i < cnt; i++) {
        float t1, t2;
        inp >> t1 >> t2;
        toruses.push_back({t1, t2});
    }

    inp >> cnt;
    for (size_t i = 0; i < cnt; i++) {
        float b1, b2, b3, b4;
        inp >> b1 >> b2 >> b3 >> b4;
        boxes.push_back({b1, b2, b3, b4});
    }

    inp >> cnt;
    for (size_t i = 0; i < cnt; i++) {
        int type, id;
        float dx, dy, dz, dw;

        inp >> type >> id >> dx >> dy >> dz >> dw;
        objects.push_back({(ObjectType) type, id, dx, dy, dz, dw});
    }

    inp >> cnt;
    for (size_t i = 0; i < cnt; i++) {
        vect<4> v;
        
        inp >> v[0] >> v[1] >> v[2] >> v[3];
        poss.push_back(v);

        inp >> v[0] >> v[1] >> v[2] >> v[3];
        dirs.push_back(v);

        inp >> v[0] >> v[1] >> v[2] >> v[3];
        ups.push_back(v);

        inp >> v[0] >> v[1] >> v[2] >> v[3];
        rigs.push_back(v);

        inp >> v[0] >> v[1] >> v[2] >> v[3];
        d4dims.push_back(v);
    }

    loadPositions(0);
}


int main(int argc, char* argv[])
{    
    cout << argc << ":" << endl;
    cout << argv[0] << ' ' << argv[1] << endl;
    readScene(argv[1]);

    glfwSetErrorCallback(error_callback);

    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    GLFWwindow* window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, key_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, cursor_pos_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    // NOTE: OpenGL error checks have been omitted for brevity

    GLuint vertex_buffer;
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLint vertex_shader = createShader("vertex_shader.glsl", GL_VERTEX_SHADER);
    GLint fragment_shader = createShader("fragment_shader.glsl", GL_FRAGMENT_SHADER);

    GLint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    GLint screenSize_location = glGetUniformLocation(program, "screenSize");

    GLint pos_location = glGetUniformLocation(program, "pos");
    GLint dir_location = glGetUniformLocation(program, "dir");
    GLint up_location = glGetUniformLocation(program, "up");
    GLint rig_location = glGetUniformLocation(program, "rig");
    GLint d4dim_location = glGetUniformLocation(program, "d4dim");

    cout << pos_location << ' ' << dir_location << ' ' << up_location << ' ' << rig_location << ' ' << d4dim_location << endl;

    GLint vpos_location = glGetAttribLocation(program, "vPos");
    GLint vcol_location = glGetAttribLocation(program, "vCol");
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*) 0);
    glEnableVertexAttribArray(vcol_location);
    glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*) (sizeof(float) * 2));

    GLint coloringFlagLocation = glGetUniformLocation(program, "coloringFlag");
    GLint reflectionFlagLocation = glGetUniformLocation(program, "reflectionFlag");
    cerr << coloringFlagLocation << ' ' << reflectionFlagLocation << endl;

    GLint spheresLocation = glGetUniformLocation(program, "spheres[0].rad");
    GLint torusesLocation = glGetUniformLocation(program, "toruses[0].t");
    GLint boxesLocation = glGetUniformLocation(program, "boxes[0].b");
    GLint hTorusesLocation = glGetUniformLocation(program, "hardToruses[0].t");
    GLint spheroCubesLocation = glGetUniformLocation(program, "spheroCubes[0].t");
    GLint objectsLocation = glGetUniformLocation(program, "objects[0].type");
    GLint objectsCountLocation = glGetUniformLocation(program, "objectsCount");

    cerr << spheresLocation  << ' ' << 
            objectsLocation  << ' ' << 
            torusesLocation  << ' ' << 
            boxesLocation    << ' ' << 
            hTorusesLocation << ' ' << 
            spheroCubesLocation << ' ' << 
            objectsCountLocation << endl;

    cout << glGetUniformLocation(program, "objects[1].delta") << endl;
    while (!glfwWindowShouldClose(window))
    {
        key_check(window);

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);

        glUniform2f(screenSize_location, (float) width, (float) height);
        glUniform4f(pos_location, pos[0], pos[1], pos[2], pos[3]);
        glUniform4f(dir_location, dir[0], dir[1], dir[2], dir[3]);
        glUniform4f(up_location,  up[0],  up[1],  up[2],  up[3]);
        glUniform4f(rig_location, rig[0], rig[1], rig[2], rig[3]);
        glUniform4f(d4dim_location, d4dim[0], d4dim[1], d4dim[2], d4dim[3]);

        glUniform1i(coloringFlagLocation, coloringFlag);
        glUniform1i(reflectionFlagLocation, reflectionFlag);

        loadArrayOfStructs(spheres, spheresLocation);
        loadArrayOfStructs(objects, objectsLocation);
        loadArrayOfStructs(toruses, torusesLocation);
        loadArrayOfStructs(toruses, hTorusesLocation);
        // loadArrayOfStructs(spheroCubes, spheroCubesLocation);
        loadArrayOfStructs(boxes, boxesLocation);
        glUniform1i(objectsCountLocation, objects.size());



        glDrawArrays(GL_TRIANGLES, 0, 6);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

//! [code]
