#pragma once

#include <initializer_list>
#include <cassert>
#include <iostream>

using namespace std;

template<size_t N>
class vect
{
public:
	vect()
	{
		for (auto& val : mData)
			val = 0;
	}

	template<typename T>
	explicit vect(std::initializer_list<T> l)
	{
		assert(l.size() == N);
		for (size_t i = 0; i < N; i++)
			mData[i] = *(l.begin() + i);
	}

	vect(vect const& other)
	{
		for (size_t i = 0; i < N; i++)
			mData[i] = other[i];
	}

	float operator[](size_t pos) const
	{
		assert(0 <= pos && pos < N);
		return mData[pos];
	}

	float& operator[](size_t pos)
	{
		assert(0 <= pos && pos < N);
		return mData[pos];
	}


	vect operator=(vect const& other)
	{
		for (size_t i = 0; i < N; i++)
			mData[i] = other[i];
	}

	vect operator+=(vect const& other)
	{
		for (size_t i = 0; i < N; i++)
			mData[i] += other[i];
		return *this;
	}

	vect operator-=(vect const& other)
	{
		for (size_t i = 0; i < N; i++)
			mData[i] -= other[i];
		return *this;
	}

	vect operator*=(double alpha)
	{
		for (size_t i = 0; i < N; i++)
			mData[i] *= alpha;
		return *this;
	}

private:
	std::array<float, N> mData;
};

template<size_t N, size_t M>
using matrix = std::array<std::array<float, M>, N>;

template<size_t N>
vect<N> operator*(vect<N> const& v, double alpha)
{
	vect<N> res = v;
	res *= alpha;
	return res;
}

template<size_t N>
vect<N> operator*(double alpha, vect<N> const& v)
{
	vect<N> res = v;
	res *= alpha;
	return res;
}

template<size_t N>
vect<N> operator+(vect<N> const& v1, vect<N> const& v2)
{
	vect<N> res = v1;
	res += v2;
	return res;
}

template<size_t N>
float operator*(vect<N> const& v1, vect<N> const& v2)
{
	float res = 0;
	for (size_t i = 0; i < N; i++)
		res += v1[i] * v2[i];
}

template<size_t N>
vect<N> operator*(float matr[N][N], vect<N> const& v)
{
	vect<N> res;
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < N; j++)
			res[i] += matr[i][j] * v[j];
	return res;
} 

vect<3> operator%(vect<3> const& v1, vect<3> const& v2)
{
	vect<3> res;
	res[0] = v1[1] * v2[2] - v1[2] * v2[1];
	res[1] = v1[2] * v2[0] - v1[0] * v2[2];
	res[2] = v1[0] * v2[1] - v1[1] * v2[0];

	return res;
}

template<size_t N>
matrix<N, N> I()
{
	matrix<N, N> result = {};
	for (size_t i = 0; i < N; i++)
		result[i][i] = 1.;
	return result;
}

template<size_t N, size_t M>
matrix<M, N> transpose(matrix<N, M> const& m)
{
	matrix<M, N> result = {};
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			result[j][i] = m[i][j];
	return result;
}

template<size_t N, size_t M>
matrix<N, M> operator+(matrix<N, M> const& m1, matrix<N, M> const& m2)
{
	matrix<N, M> result = {};
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < M; j++)
			result[i][j] = m1[i][j] + m2[i][j];
	return result;
}

template<size_t N, size_t M>
matrix<N, M> operator-(matrix<N, M> const& m1, matrix<N, M> const& m2)
{
	matrix<N, M> result = {};
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < M; j++)
			result[i][j] = m1[i][j] - m2[i][j];
	return result;
}

template<size_t N, size_t M>
matrix<N, M> operator*(float a, matrix<N, M> const& m)
{
	matrix<N, M> result = m;
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < M; j++)
			result[i][j] = m[i][j] * a;
	return result;
}

template<size_t N, size_t K, size_t M>
matrix<N, M> operator*(matrix<N, K> const& m1, matrix<K, M> const& m2)
{
	matrix<N, M> result = {};
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < M; j++)
			for (size_t k = 0; k < K; k++)
				result[i][j] += m1[i][k] * m2[k][j];
	return result;
}

template<size_t N, size_t M>
vect<N> operator*(matrix<N, M> const& m, vect<M> const& v)
{
	vect<N> result = {};
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < M; j++)
			result[i] += m[i][j] * v[j];
	return result;
}

template<size_t N>
matrix<N, N> outerProduct(vect<N> const& u, vect<N> const& v)
{
	matrix<N, N> result = {};
	for (size_t i = 0; i < N; i++)
		for (size_t j = 0; j < N; j++)
			result[i][j] = u[i] * v[j];
	return result;
}

template<size_t N>
matrix<N, N> wedgeProduct(vect<N> const& u, vect<N> const& v)
{
	return outerProduct(u, v) - outerProduct(v, u);
}


matrix<4, 4> proj(vect<4> const&u, vect<4> const& v)
{
	matrix<4, 2> At = {std::array<float, 2>{u[0], v[0]}, std::array<float, 2>{u[1], v[1]}, 
					   std::array<float, 2>{u[2], v[2]}, std::array<float, 2>{u[3], v[3]}};
	return At * transpose(At); 
}

matrix<4, 4> rotationMatrix(vect<4> const& u, vect<4> const& v, double theta)
{
	return I<4>() - (1 - cos(theta)) * proj(u, v) + sin(theta) * wedgeProduct(u, v);
	// matrix<4, 4> 
}

void rotate(vect<3>& v, vect<3> const& u, float alpha, bool flag=false, vect<3> f={}, vect<3> s={})
{
	// matrix<3, 3> L = {std::array<float, 3>{0, -u[2], u[1]}, std::array<float, 3>{u[2], 0, -u[0]}, std::array<float, 3>{-u[1], u[0], 0}};

	if (flag) {
		auto matr = cos(alpha) * I<3>() + 
					(1 - cos(alpha)) * outerProduct(u, u) + 
					sin(alpha) * matrix<3, 3>({std::array<float, 3>{0, -u[2], u[1]}, 
											   std::array<float, 3>{u[2], 0, -u[0]}, 
											   std::array<float, 3>{-u[1], u[0], 0}});

		cout << "Matrix :" << endl;
		for (int i = 0; i < 3; i++, cout << endl)
			for (int j = 0; j < 3; j++)
				printf("%10.10f ", matr[i][j]);

		vect<4> u4{f[0], f[1], f[2], 0.0f};
		vect<4> v4{s[0], s[1], s[2], 0.0f}; 
		auto matr44 = rotationMatrix(u4, v4, alpha);
		cout << "Matrix 4x4:" << endl;
		for (int i = 0; i < 4; i++, cout << endl)
			for (int j = 0; j < 4; j++)
				printf("%10.10f ", matr44[i][j]);
		cout << endl << endl;
	}
	v = cos(alpha) * v + (1 - cos(alpha)) * (u * v) * u + sin(alpha) * (u % v);
}

void simpleRotation(vect<4>& u, vect<4>& v, float theta)
{
	auto matr = rotationMatrix(u, v, theta);
	u = matr * u;
	v = matr * v;
}