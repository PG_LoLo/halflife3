#version 430
#extension GL_ARB_shading_language_420pack : require

#define SPHERE_TYPE 0
#define TORUS_TYPE 1
#define BOX_TYPE 2
#define HARD_TORUS_TYPE 3
#define SPHERO_CUBE_TYPE 4

uniform vec2 screenSize;

uniform vec4 pos;
uniform vec4 dir;
uniform vec4 up;
uniform vec4 rig;
uniform vec4 d4dim;

uniform bool coloringFlag;
uniform bool reflectionFlag;

const int MAX_ITERS = 500;
const float MAX_LEN = 1e5;
const float EPS = 1e-3;

const float h = 2.;

const int LIGHTS_CNT = 1;

vec4 LIGHTS[5] = {vec4(100., 0., 0., 0.),
	              vec4(0., -1000., -1000., 0.),
	              vec4(0., 100., 0., 0.), 
	              vec4(0., 0., 100., 0.), 
	              vec4(0., 0., 0., 100.)};

struct SphereData
{
	float rad;
};

struct TorusData
{
	vec2 t;
};

struct SpheroCubeData
{
	vec3 t;
};

struct HardTorusData
{
	vec2 t;
};

struct BoxData
{
	vec4 b;
};

struct ObjectData
{
	int type;
	int id;
	vec4 delta;
};

const int N = 100;
layout(location = 1)    uniform ObjectData objects[N];
layout(location = 1000) uniform SphereData spheres[N];
layout(location = 2000) uniform TorusData toruses[N];
layout(location = 3000) uniform BoxData boxes[N];
layout(location = 4000) uniform HardTorusData hardToruses[N];
// layout(location = 5000) uniform SpheroCubeData spheroCubes[N];
uniform int objectsCount;
// uniform ObjectData objects[N];

const vec4 REP = vec4(100, 100, 100, 4);

float calcSphereDist(vec4 pos, SphereData sphere)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	return abs(length(pos) - sphere.rad);
}

vec4 calcSphereNorm(vec4 pos, SphereData sphere)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	return pos / length(pos);
}

float calcSpheroCubeDist(vec4 pos, SpheroCubeData spheroCube)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	vec3 flatted = vec3(pos.x, pos.y, pos.z);
	float sphereDist = length(flatted) - spheroCube.t.y;
	vec3 temp = abs(flatted) - vec3(spheroCube.t.x / 2., spheroCube.t.x / 2., spheroCube.t.x / 2.);
	float cubeDist   = min(max(max(temp.x, temp.y), temp.z), 0.0) + length(max(temp, 0.0));
	float delta      = cubeDist - sphereDist;
	float sinPhi     = spheroCube.t.z / sqrt(spheroCube.t.z * spheroCube.t.z + delta * delta);
	float alpha      = sphereDist + (cubeDist - sphereDist) * (pos.w / spheroCube.t.z);
	float h = alpha / sinPhi;
	vec4 downShifted = vec4(pos.x, pos.y, pos.z, 0.);
	vec4 horS = downShifted * (length(downShifted) - sphereDist) / length(downShifted);
	vec4 horC = downShifted * (length(downShifted) - cubeDist) / length(downShifted) + vec4(0., 0., 0., spheroCube.t.z);
	vec4 ls = pos - horS;
	vec4 lc = pos - horC;
	vec4 line = horC - horS;
	if (h <= 0 && pos.w >= 0 && pos.w <= spheroCube.t.z)
	{
		return max(h, max(-pos.w, pos.w - spheroCube.t.z));
	}
	else if (pos.w < 0 && sphereDist <= 0)
	{
		return -pos.w;
	}
	else if (pos.w > spheroCube.t.z && cubeDist <= 0)
	{
		return pos.w - spheroCube.t.z;
	}
	else if (dot(line, ls) >= 0 && dot(line, lc) <= 0)
	{
		return h;
	}
	else if (dot(line, ls) < 0)
	{
		return length(pos - horS);
	}
	else if (dot(line, lc) > 0)
	{
		return length(pos - horC);
	}
	// return cubeDist;
}

vec4 calcSpheroCubeNorm(vec4 pos, SpheroCubeData spheroCube)
{	
	// pos = mod(pos, REP) - 0.5 * REP;
	if (abs(pos.w) < EPS)
	{
		return vec4(0., 0., 0., -1.);
	}
	else if (abs(pos.w - spheroCube.t.z) < EPS)
	{
		return vec4(0., 0., 0., 1.);
	}
	else 
	{		
		vec4 downShifted = vec4(pos.x, pos.y, pos.z, 0.);
		vec4 horS = downShifted * spheroCube.t.y / length(downShifted);
		vec4 line = pos - horS;
		float y = - line.w / (line.y + line.x * horS.x / horS.y + line.z * horS.z / horS.y);
		float x = y * horS.x / horS.y;
		float z = y * horS.z / horS.y;
		vec4 h = vec4(x, y, z, 1.);
		return h / length(h);
	}
}

float calcTorusDist(vec4 pos, TorusData torus)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	vec2 q = vec2(length(pos.xyw) - torus.t.x, pos.z);
  	return abs(length(q) - torus.t.y);
}

vec4 calcTorusNorm(vec4 pos, TorusData torus)
{
	// pos = mod(pos, REP) - 0.5 * REP;

	float t = torus.t.x;
	float value = sqrt(dot(pos, pos) + t * t + 2 * length(pos.xyw) * t);
	vec4 res = vec4((pos.x - t * pos.x / length(pos.xyw)) / value, 
				    (pos.y - t * pos.y / length(pos.xyw)) / value, 
				     pos.z / value, 
				    (pos.w - t * pos.w / length(pos.xyw)) / value
				    );
	
	return res / length(res);
}

float calcHardTorusDist(vec4 pos, HardTorusData torus)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	vec3 q = vec3(length(pos.xy) - torus.t.x, pos.zw);

  	return abs(length(q) - torus.t.y);
}

vec4 calcHardTorusNorm(vec4 pos, HardTorusData torus)
{
	// pos = mod(pos, REP) - 0.5 * REP;

	float t = torus.t.x;
	float value = sqrt(dot(pos, pos) + t * t + 2 * length(pos.xy) * t);
	vec4 res = vec4((pos.x - t * pos.x / length(pos.xy)) / value, 
				    (pos.y - t * pos.y / length(pos.xy)) / value, 
				     pos.z / value, 
				     pos.w / value
				    );
	
	return res / length(res);
}

float calcBoxDist(vec4 pos, BoxData box)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	vec4 d = abs(pos) - box.b;
	return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}

vec4 calcBoxNorm(vec4 pos, BoxData box)
{
	// pos = mod(pos, REP) - 0.5 * REP;
	// // pos -= box.b * 0.5;
	// float max_value = max(abs(pos.x), max(abs(pos.y), abs(pos.z)));
	// vec3 norm = vec3(0.0);

	// if (abs(pos.x) == max_value)
	// 	norm.x = sign(pos.x);
	// if (abs(pos.y) == max_value)
	// 	norm.y = sign(pos.y);
	// if (abs(pos.z) == max_value)
	// 	norm.z = sign(pos.z);

	// return norm / length(norm);
	return pos;
}

float calcDist(vec4 pos, int type, int id)
{
	if (type == SPHERE_TYPE) {
		return calcSphereDist(pos, spheres[id]);
	}
	else if (type == TORUS_TYPE) {
		return calcTorusDist(pos, toruses[id]);
	} 
	else if (type == BOX_TYPE) {
		return calcBoxDist(pos, boxes[id]);
	} 
	else if (type == HARD_TORUS_TYPE) {
		return calcHardTorusDist(pos, hardToruses[id]);
	} 
	// else if (type == SPHERO_CUBE_TYPE) {
	// 	return calcSpheroCubeDist(pos, spheroCubes[id]);
	// } 
}

vec4 calcNorm(vec4 pos, int type, int id)
{
	if (type == SPHERE_TYPE) {
		return calcSphereNorm(pos, spheres[id]);
	}
	else if (type == TORUS_TYPE) {
		return calcTorusNorm(pos, toruses[id]);
	} 
	else if (type == BOX_TYPE) {
		return calcBoxNorm(pos, boxes[id]);
	} 
	else if (type == HARD_TORUS_TYPE) {
		return calcHardTorusNorm(pos, hardToruses[id]);
	} 
	// else if (type == SPHERO_CUBE_TYPE) {
	// 	return calcSpheroCubeNorm(pos, spheroCubes[id]);
	// } 
}

struct MinDistResult
{
	float dist;
	int objectId;
};

MinDistResult findClosest(vec4 pos)
{
	int num = -1;
	float min_value;

	for (int i = 0; i < objectsCount; i++) {
		float dist = calcDist(pos - objects[i].delta, objects[i].type, objects[i].id);
	
		if (num == -1 || min_value > dist) {
			num = i;
			min_value = dist;
		}
	}

	return MinDistResult(min_value, num);
}

struct RayTraceResult
{
	vec4 pos;
	int objectId;
};

RayTraceResult raytrace(vec4 pos, vec4 v)
{
	for (int i = 0; length(pos) < MAX_LEN && i < MAX_ITERS; i++) {
		MinDistResult minDist = findClosest(pos);
		if (minDist.dist < EPS)
			return RayTraceResult(pos, minDist.objectId);
	
		pos += minDist.dist * v;
	}

	return RayTraceResult(vec4(0.0), -1);
}

vec3 posToColor(vec4 pos)
{
	if (coloringFlag) {
		int val = int(floor(pos.x / 0.1) + floor(pos.y / 0.1) + floor(pos.z / 0.1) + floor(pos.w / 0.1));
		if (val % 2 == 0)
			return vec3(1., 1., 1.);
		else
			return vec3(1., 0., 0.) ;
	}
	else
		return vec3(1., 1., 1.);
}

vec3 calcColor(vec4 r, vec4 norm)
{
	r /= length(r);
	float light_sum = 0;

	for (int i = 0; i < LIGHTS_CNT; i++) {
    	vec4 to_light = (LIGHTS[i] - r) / length(LIGHTS[i] - r);
	
		float light_cos = dot(norm, to_light);
		if (light_cos > 0)
			light_sum += light_cos;
		else
			light_sum += -light_cos * 0.3;
	}

	vec3 color = posToColor(r);
	return vec3(color * light_sum / (LIGHTS_CNT + 1));

}

vec4 toOurProjection(vec4 v)
{
	return v - dot(v, d4dim) * d4dim;
}

float calcLightning3d(vec4 r, vec4 norm)
{
	norm = toOurProjection(norm);
	norm /= length(norm);

	float light_sum = 0;
	for (int i = 0; i < LIGHTS_CNT; i++) {
    	vec4 to_light = (LIGHTS[i] - r) / length(LIGHTS[i] - r);
    	to_light = toOurProjection(to_light);
    	to_light /= length(to_light);

		float light_cos = dot(norm, to_light);
		if (light_cos > 0)
			light_sum += light_cos;
		else
			light_sum += -light_cos * 0.3;
	}

	float alpha = 0.3;
	return (alpha + (1 - alpha) * light_sum / LIGHTS_CNT);
}


vec4 findColor(vec4 pos, vec4 v)
{
	RayTraceResult result = raytrace(pos, v);

    if (result.objectId == -1) 
    	return vec4(0.);

    ObjectData obj = objects[result.objectId];
	vec4 norm = calcNorm(result.pos - obj.delta, obj.type, obj.id);
	
	float first_light = calcLightning3d(result.pos - obj.delta, norm);
	vec4 first_color = vec4(posToColor(result.pos - obj.delta) * first_light, 1.);
	// return vec4(first_color, 1.);

	if (!reflectionFlag)
		return first_color;

	v = -v;
	vec4 vn = dot(norm, v) * norm;
	vec4 new_v = 2 * vn - v;
	new_v /= length(new_v);

	RayTraceResult refl = raytrace(result.pos + 2 * EPS * new_v, new_v);
	if (refl.objectId == -1)
		return first_color;

	ObjectData reflObj = objects[result.objectId];
	vec4 reflNorm = calcNorm(refl.pos - reflObj.delta, reflObj.type, reflObj.id);
	float second_light = calcLightning3d(refl.pos - reflObj.delta, reflNorm);
	vec4 second_color = vec4(posToColor(refl.pos - reflObj.delta) * second_light, 1.);

	float alpha = 0.5;
	return alpha * first_color + (1 - alpha) * second_color;
}

void main()
{
	// if (objectsCount == 3.) {
	//     gl_FragColor = vec4(1., 0., 0., 1.);       
	// 	return;
	// }
	// if (spheres[0].rad == 1. && objects[0].id == 0 && objects[0].type == 0 && objects[2].delta.x == 0.) {
	//     gl_FragColor = vec4(1., 0., 0., 1.);       
	// 	return;
	// }

    float x = (gl_FragCoord.x / screenSize.x * 2. - 1.) * screenSize.x / screenSize.y;
    float y = gl_FragCoord.y / screenSize.y * 2. - 1.;
    
    vec4 v = h * dir + y * up - x * rig;
    v /= length(v);
    
    gl_FragColor = findColor(pos, v);
}
