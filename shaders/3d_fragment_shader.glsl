#version 430

#define SPHERE_TYPE 0
#define TORUS_TYPE 1
#define BOX_TYPE 2

uniform vec2 screenSize;

uniform vec3 pos;
uniform vec3 dir;
uniform vec3 up;
uniform vec3 rig;

const float h = 2.;

const vec3 LIGHT = vec3(-10, -10, -10);

struct SphereData
{
	float rad;
};

struct TorusData
{
	vec2 t;
};

struct BoxData
{
	vec3 b;
};

struct ObjectData
{
	int type;
	int id;
	vec3 delta;
};

const int N = 100;
layout(location = 1) uniform ObjectData objects[N];
layout(location = 1000) uniform SphereData spheres[N];
layout(location = 2000) uniform TorusData toruses[N];
layout(location = 3000) uniform BoxData boxes[N];
uniform int objectsCount;
// uniform ObjectData objects[N];

const vec3 REP = vec3(100, 100, 100);

float calcSphereDist(vec3 pos, SphereData sphere)
{
	pos = mod(pos, REP) - 0.5 * REP;
	return length(pos) - sphere.rad;
}

vec3 calcSphereNorm(vec3 pos, SphereData sphere)
{
	pos = mod(pos, REP) - 0.5 * REP;
	return pos / length(pos);
}

float calcTorusDist(vec3 pos, TorusData torus)
{
	pos = mod(pos, REP) - 0.5 * REP;
	
	// float c = cos(20.0 * pos.y);
 //    float s = sin(20.0 * pos.y);
 //    mat2  m = mat2(c, -s, s, c);
 //    pos = vec3(m * pos.xz, pos.y);
    
	vec2 q = vec2(length(pos.xy) - torus.t.x, pos.z);
  	return length(q) - torus.t.y;
}

vec3 calcTorusNorm(vec3 pos, TorusData torus)
{
	pos = mod(pos, REP) - 0.5 * REP;
	
	// float c = cos(20.0 * pos.y);
 //    float s = sin(20.0 * pos.y);
 //    mat2  m = mat2(c, -s, s, c);
 //    pos = vec3(m * pos.xz, pos.y);
    
	float t = torus.t.x;
	float value = sqrt(dot(pos, pos) + t * t + 2 * length(pos.xy) * t);
	vec3 res = vec3((pos.x - t * pos.x / length(pos.xy)) / value, (pos.y - t * pos.y / length(pos.xy)) / value, pos.z / value);
	return res / length(res);
}

float calcBoxDist(vec3 pos, BoxData box)
{
	pos = mod(pos, REP) - 0.5 * REP;
	vec3 d = abs(pos) - box.b;
	return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}

vec3 calcBoxNorm(vec3 pos, BoxData box)
{
	pos = mod(pos, REP) - 0.5 * REP;
	// pos -= box.b * 0.5;
	float max_value = max(abs(pos.x), max(abs(pos.y), abs(pos.z)));
	vec3 norm = vec3(0.0);

	if (abs(pos.x) == max_value)
		norm.x = sign(pos.x);
	if (abs(pos.y) == max_value)
		norm.y = sign(pos.y);
	if (abs(pos.z) == max_value)
		norm.z = sign(pos.z);

	return norm / length(norm);
}

float calcDist(vec3 pos, int type, int id)
{
	if (type == SPHERE_TYPE) {
		return calcSphereDist(pos, spheres[id]);
	}
	else if (type == TORUS_TYPE) {
		return calcTorusDist(pos, toruses[id]);
	} 
	else if (type == BOX_TYPE) {
		return calcBoxDist(pos, boxes[id]);
	} 
}

vec3 calcNorm(vec3 pos, int type, int id)
{
	if (type == SPHERE_TYPE) {
		return calcSphereNorm(pos, spheres[id]);
	}
	else if (type == TORUS_TYPE) {
		return calcTorusNorm(pos, toruses[id]);
	} 
	else if (type == BOX_TYPE) {
		return calcBoxNorm(pos, boxes[id]);
	} 
}

struct MinDistResult
{
	float dist;
	int objectId;
};

MinDistResult findClosest(vec3 pos)
{
	int num = -1;
	float min_value;

	for (int i = 0; i < objectsCount; i++) {
		float dist = calcDist(pos - objects[i].delta, objects[i].type, objects[i].id);
	
		if (num == -1 || min_value > dist) {
			num = i;
			min_value = dist;
		}
	}

	return MinDistResult(min_value, num);
}

const int MAX_ITERS = 100;
const float MAX_LEN = 1e5;
const float EPS = 1e-3;

struct RayTraceResult
{
	vec3 pos;
	int objectId;
};

RayTraceResult raytrace(vec3 pos, vec3 v)
{
	for (int i = 0; length(pos) < MAX_LEN && i < MAX_ITERS; i++) {
		MinDistResult minDist = findClosest(pos);
		if (minDist.dist < EPS)
			return RayTraceResult(pos, minDist.objectId);
	
		pos += minDist.dist * v;
	}

	return RayTraceResult(vec3(0.0), -1);
}

void main()
{
	// if (objectsCount == 3.) {
	//     gl_FragColor = vec4(1., 0., 0., 1.);       
	// 	return;
	// }
	// if (spheres[0].rad == 1. && objects[0].id == 0 && objects[0].type == 0 && objects[2].delta.x == 0.) {
	//     gl_FragColor = vec4(1., 0., 0., 1.);       
	// 	return;
	// }

    float x = gl_FragCoord.x / screenSize.x * 2. - 1.;
    float y = gl_FragCoord.y / screenSize.y * 2. - 1.;
    
    vec3 v = h * dir + y * up - x * rig;
    v /= length(v);
    
    RayTraceResult result = raytrace(pos, v);

    if (result.objectId != -1) {
    	ObjectData obj = objects[result.objectId];
    	
    	vec3 pos = result.pos;
		vec3 norm = calcNorm(pos - obj.delta, obj.type, obj.id);
		vec3 to_light = (LIGHT - pos) / length(LIGHT - pos);

		// float light = max(0.1, dot(norm, to_light));
		float light_cos = dot(norm, to_light);
		float light = 0.;
		if (light_cos > 0)
			light += light_cos;
		else
			light += -light_cos * 0.1;
		light = (light + 0.1) / 1.1;
	    gl_FragColor = vec4(vec3(1., 1., 1.) * light, 1.);
	}
    else
	    gl_FragColor = vec4(0.);       
}
