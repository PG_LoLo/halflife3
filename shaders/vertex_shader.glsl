uniform mat4 MVP;
uniform vec2 screenSize;

attribute vec3 vCol;
attribute vec2 vPos;
varying vec3 color;

void main()
{
    gl_Position = vec4(vPos, 0.0, 1.0);
    color = vCol;
}