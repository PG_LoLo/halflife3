# README #

## Requirements

```sudo apt install libglfw3-dev```

or install glfw from here: http://www.glfw.org/

You need support of GLSL 4.30.

## Install 

```
cmake .
make
```

## Run 

```
./hl3 <path to scene>
```

where `<path to scene>` is path to one of the files in `predefines/`. 